#Project Title
Grupo Dot Shopping Center

#Getting Started
To run the project use 
```
mvn spring-boot:run -Dspring-boot.run.profiles=default
mvn spring-boot:run -Dspring-boot.run.profiles=dev
mvn spring-boot:run -Dspring-boot.run.profiles=test
mvn spring-boot:run -Dspring-boot.run.profiles=qa
```

#Prerequisites
-Maven command line
-Mysql local database with the following properties
```
user : root  
pass : admin
database: shopping_center
```

#Running the tests
-mvn clean package

#Api Documentation
-http://localhost:8080/swagger-ui.html

#Postman collection
-Postman collection is present on project \src\test\resources

#Disable security
-To disable security change the property security.enabled=false on application.properties

#Security
-Authentication endpoint
-EndPoint http://localhost:8080/auth/v1/login
-Body:
```
{
	"idNumber":"1033726",
	"password":"123456"
}
```
-Method: POST

-The token information is present on the header : token

-One admin user is created by default.
```
User: 1033726
pass: 123456
```

#Built With
Maven - Dependency Management