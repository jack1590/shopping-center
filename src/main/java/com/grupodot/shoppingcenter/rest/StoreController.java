package com.grupodot.shoppingcenter.rest;


import com.grupodot.shoppingcenter.contants.ApplicationConstants;
import com.grupodot.shoppingcenter.dto.Store;
import com.grupodot.shoppingcenter.dto.ValidProductStore;
import com.grupodot.shoppingcenter.dto.seacrh.store.SearchStoreRequest;
import com.grupodot.shoppingcenter.service.StoreService;
import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@PreAuthorize("hasRole('ADMIN')")
@RestController
@RequestMapping(ApplicationConstants.RESOURCE_VERSION_1 + ApplicationConstants.STORE_RESOURCE_NAME)
public class StoreController {

  private static final Logger LOGGER = LoggerFactory.getLogger(StoreController.class);

  @Autowired
  private StoreService storeService;

  @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<Store> create(@Validated @RequestBody Store store) {
    LOGGER.info("Begin create store controller");

    Long id = storeService.create(store);

    URI storeLocation = ServletUriComponentsBuilder.fromUriString(ApplicationConstants.RESOURCE_VERSION_1 + ApplicationConstants.STORE_RESOURCE_NAME)
        .path("/{id}")
        .buildAndExpand(id)
        .toUri();

    LOGGER.info("End create store controller");

    return ResponseEntity.created(storeLocation)
        .header("Access-Control-Expose-Headers", "Location")
        .build();
  }

  @PutMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<Store> update(@Validated @RequestBody Store store) {
    LOGGER.info("Begin update store controller");

    storeService.update(store);
    Store savedStore = storeService.getById(store.getId());
    ResponseEntity<Store> re = ResponseEntity.ok(savedStore);

    LOGGER.info("End update store controller");
    return re;
  }

  @GetMapping(path = "/{store-id}", produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<Store> get(@PathVariable("store-id") Long storeId) {
    LOGGER.info("Begin get store controller");

    Store store = storeService.getById(storeId);

    ResponseEntity<Store> re = ResponseEntity.ok(store);

    LOGGER.info("End get store controller");
    return re;
  }

  @DeleteMapping(path = "/{store-id}", produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity delete(@PathVariable("store-id") Long storeId) {
    LOGGER.info("Begin delete store controller");

    storeService.delete(storeId);

    LOGGER.info("End delete store controller");
    return ResponseEntity.ok()
        .build();
  }

  @PostMapping(path = "/search", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<Page<Store>> search(@RequestBody SearchStoreRequest params) {
    return ResponseEntity.ok(storeService.search(params));
  }

  @PostMapping(path = "/associate", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity search(@Validated @RequestBody ValidProductStore validProductStore) {
    storeService.associate(validProductStore.getProductStores());
    return ResponseEntity.ok("Products associated");
  }

}
