package com.grupodot.shoppingcenter.rest;

import com.grupodot.shoppingcenter.contants.ApplicationConstants;
import com.grupodot.shoppingcenter.dto.Product;
import com.grupodot.shoppingcenter.dto.seacrh.product.SearchProductRequest;
import com.grupodot.shoppingcenter.service.ProductService;
import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping(ApplicationConstants.RESOURCE_VERSION_1 + ApplicationConstants.PRODUCT_RESOURCE_NAME)
public class ProductController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

  @Autowired
  private ProductService productService;

  @PreAuthorize("hasRole('ADMIN')")
  @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<Product> create(@Validated @RequestBody Product product) {
    LOGGER.info("Begin create product controller");

    Long id = productService.create(product);

    URI productLocation =
        ServletUriComponentsBuilder.fromUriString(ApplicationConstants.RESOURCE_VERSION_1 + ApplicationConstants.PRODUCT_RESOURCE_NAME)
            .path("/{id}")
            .buildAndExpand(id)
            .toUri();

    LOGGER.info("End create product controller");

    return ResponseEntity.created(productLocation)
        .header("Access-Control-Expose-Headers", "Location")
        .build();
  }

  @PreAuthorize("hasRole('ADMIN')")
  @PutMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<Product> update(@Validated @RequestBody Product product) {
    LOGGER.info("Begin update product controller");

    productService.update(product);
    Product savedProduct = productService.getById(product.getId());
    ResponseEntity<Product> re = ResponseEntity.ok(savedProduct);

    LOGGER.info("End update product controller");
    return re;
  }

  @GetMapping(path = "/{product-id}", produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<Product> get(@PathVariable("product-id") Long productId) {
    LOGGER.info("Begin get product controller");

    Product product = productService.getById(productId);

    ResponseEntity<Product> re = ResponseEntity.ok(product);

    LOGGER.info("End get product controller");
    return re;
  }

  @PreAuthorize("hasRole('ADMIN')")
  @DeleteMapping(path = "/{product-id}", produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity delete(@PathVariable("product-id") Long productId) {
    LOGGER.info("Begin delete product controller");

    productService.delete(productId);

    LOGGER.info("End delete product controller");
    return ResponseEntity.ok()
        .build();
  }

  @PostMapping(path = "/search", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<Page<Product>> search(@RequestBody SearchProductRequest params) {
    return ResponseEntity.ok(productService.search(params));
  }

}
