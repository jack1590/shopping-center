package com.grupodot.shoppingcenter.rest;

import com.grupodot.shoppingcenter.contants.ApplicationConstants;
import com.grupodot.shoppingcenter.service.OpenBankService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApplicationConstants.OPENBANK_RESOURCE_NAME)
public class OpenBankController {

  private static final Logger LOGGER = LoggerFactory.getLogger(OpenBankController.class);

  @Autowired
  private OpenBankService openBankService;

  @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity get() {
    return ResponseEntity.ok(openBankService.getProducts());
  }
}
