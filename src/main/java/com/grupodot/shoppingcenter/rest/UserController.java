package com.grupodot.shoppingcenter.rest;

import com.grupodot.shoppingcenter.contants.ApplicationConstants;
import com.grupodot.shoppingcenter.dto.User;
import com.grupodot.shoppingcenter.dto.seacrh.user.SearchUserRequest;
import com.grupodot.shoppingcenter.service.UserService;
import java.net.URI;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@PreAuthorize("hasRole('ADMIN')")
@RestController
@RequestMapping(ApplicationConstants.RESOURCE_VERSION_1 + ApplicationConstants.USER_RESOURCE_NAME)
public class UserController {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

  @Autowired
  private UserService userService;

  @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<User> create(HttpServletRequest request, @Validated @RequestBody User user) {
    LOGGER.info("Begin create user controller");

    Long id = userService.create(user);

    URI userLocation = ServletUriComponentsBuilder.fromUriString(ApplicationConstants.RESOURCE_VERSION_1 + ApplicationConstants.USER_RESOURCE_NAME)
        .path("/{id}")
        .buildAndExpand(id)
        .toUri();

    LOGGER.info("End create user controller");

    return ResponseEntity.created(userLocation)
        .header("Access-Control-Expose-Headers", "Location")
        .build();
  }

  @PutMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<User> update(@Validated @RequestBody User user) {
    LOGGER.info("Begin update user controller");

    userService.update(user);
    User savedUser = userService.getById(user.getId());
    ResponseEntity<User> re = ResponseEntity.ok(savedUser);

    LOGGER.info("End update user controller");
    return re;
  }

  @GetMapping(path = "/{user-id}", produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<User> get(@PathVariable("user-id") Long userId) {
    LOGGER.info("Begin get user controller");

    User user = userService.getById(userId);

    ResponseEntity<User> re = ResponseEntity.ok(user);

    LOGGER.info("End get user controller");
    return re;
  }

  @DeleteMapping(value = "/{user-id}", produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity delete(@PathVariable("user-id") Long userId) {
    LOGGER.info("Begin delete user controller");

    userService.delete(userId);

    LOGGER.info("End delete user controller");
    return ResponseEntity.ok()
        .build();
  }

  @PostMapping(path = "/search", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<Page<User>> search(@RequestBody SearchUserRequest params) {
    return ResponseEntity.ok(userService.search(params));
  }

}
