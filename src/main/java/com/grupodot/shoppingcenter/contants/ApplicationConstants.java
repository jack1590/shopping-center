package com.grupodot.shoppingcenter.contants;

public class ApplicationConstants {

  // RESOURCES
  public static final String RESOURCE_VERSION_1 = "/v1";
  public static final String USER_RESOURCE_NAME = "/users";
  public static final String STORE_RESOURCE_NAME = "/stores";
  public static final String PRODUCT_RESOURCE_NAME = "/products";
  public static final String OPENBANK_RESOURCE_NAME = "/open-bank-products";


  //Security SETTINGS
  public static final String AUTHENTICATION_HEADER_NAME = "Authorization";
  public static final String AUTHENTICATION_URL = "/auth/v1/login";
  public static final String REFRESH_TOKEN_URL = "/auth/v1/token";
  public static final String API_ROOT_URL = "/v1/**";
}
