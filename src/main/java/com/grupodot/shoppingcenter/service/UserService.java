package com.grupodot.shoppingcenter.service;

import com.grupodot.shoppingcenter.dto.User;
import com.grupodot.shoppingcenter.dto.seacrh.user.SearchUserRequest;
import org.springframework.data.domain.Page;

public interface UserService {

  Long create(User user);

  void update(User user);

  User getById(Long id);

  void delete(Long id);

  Page<User> search(SearchUserRequest params);
}
