package com.grupodot.shoppingcenter.service;

import com.grupodot.shoppingcenter.dto.ProductStore;
import com.grupodot.shoppingcenter.dto.Store;
import com.grupodot.shoppingcenter.dto.seacrh.store.SearchStoreRequest;
import java.util.Set;
import org.springframework.data.domain.Page;

public interface StoreService {

  Long create(Store store);

  void update(Store store);

  Store getById(Long id);

  void delete(Long storeId);

  Page<Store> search(SearchStoreRequest params);

  void associate(Set<ProductStore> products);
}
