package com.grupodot.shoppingcenter.service;

import com.grupodot.shoppingcenter.dto.Product;
import com.grupodot.shoppingcenter.dto.seacrh.product.SearchProductRequest;
import org.springframework.data.domain.Page;

public interface ProductService {

  Long create(Product product);

  void update(Product product);

  Product getById(Long id);

  void delete(Long productId);

  Page<Product> search(SearchProductRequest params);
}
