package com.grupodot.shoppingcenter.service.impl;

import com.grupodot.shoppingcenter.dao.ProductStoreDao;
import com.grupodot.shoppingcenter.dao.StoreDao;
import com.grupodot.shoppingcenter.dao.builder.StoreSearchQueryBuilder;
import com.grupodot.shoppingcenter.dto.ProductStore;
import com.grupodot.shoppingcenter.dto.Store;
import com.grupodot.shoppingcenter.dto.seacrh.store.SearchStoreQueryParameters;
import com.grupodot.shoppingcenter.dto.seacrh.store.SearchStoreRequest;
import com.grupodot.shoppingcenter.entity.StoreEntity;
import com.grupodot.shoppingcenter.mapper.ProductStoreMapper;
import com.grupodot.shoppingcenter.mapper.StoreMapper;
import com.grupodot.shoppingcenter.service.StoreService;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StoreServiceImpl implements StoreService {

  @Autowired
  private StoreDao dao;

  @Autowired
  private ProductStoreDao productStoreDao;

  @Autowired
  private StoreMapper mapper;

  @Autowired
  private ProductStoreMapper productStoreMapper;

  @Autowired
  private StoreSearchQueryBuilder storeSearchQueryBuilder;

  @Override
  @Transactional
  public Long create(Store store) {
    StoreEntity entity = mapper.toStoreEntity(store);
    validateUniqueCode(store.getCode());

    return dao.save(entity)
        .getId();
  }

  @Override
  @Transactional
  public void update(Store store) {
    validateUniqueCode(store.getCode());
    validateEntityExists(store.getId());

    StoreEntity entity = mapper.toStoreEntity(store);
    dao.save(entity);
  }

  @Override
  @Transactional(readOnly = true)
  public Store getById(Long id) {
    Optional<StoreEntity> entityOpt = dao.findById(id);
    if (entityOpt.isPresent()) {
      return mapper.toStore(entityOpt.get());
    } else {
      throw new EntityNotFoundException("Store not found");
    }
  }

  @Override
  @Transactional
  public void delete(Long storeId) {
    validateEntityExists(storeId);
    StoreEntity entity = mapper.toStoreEntity(getById(storeId));
    dao.delete(entity);
  }

  @Override
  public Page<Store> search(SearchStoreRequest params) {
    Pageable pageable = params.createPageRequest();
    Page<StoreEntity> entities = dao.findAll(storeSearchQueryBuilder.whereClauseFromSearchParameters(params), pageable);

    return entities.map(entity -> mapper.toStore(entity));
  }

  @Override
  public void associate(Set<ProductStore> products) throws ValidationException {

    productStoreDao.saveAll(products.stream()
        .map(productStoreMapper::toProductStoreEntity)
        .collect(Collectors.toSet()));
  }

  public void validateUniqueCode(String code) {
    SearchStoreQueryParameters searchStoreQueryParameters = new SearchStoreQueryParameters();
    searchStoreQueryParameters.setCode(code);

    SearchStoreRequest params = new SearchStoreRequest();
    params.setPage(0);
    params.setSize(1);
    params.setSearch(searchStoreQueryParameters);

    if (this.search(params)
        .getNumberOfElements() > 0) {
      throw new ValidationException("Code already registered");
    }
  }

  public void validateEntityExists(Long storeId) {
    if (storeId == null || getById(storeId) == null) {
      throw new EntityNotFoundException("Store not found");
    }
  }
}
