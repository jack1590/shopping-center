package com.grupodot.shoppingcenter.service.impl;

import com.grupodot.shoppingcenter.service.OpenBankService;
import java.util.Map;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OpenBankServiceImpl implements OpenBankService {

  @Autowired
  private ProducerTemplate producerTemplate;

  @Override
  public Map<String, Object> getProducts() {
    return (Map<String, Object>) producerTemplate.sendBody("direct:getRestFromOpenBank", ExchangePattern.InOut, null);
  }
}
