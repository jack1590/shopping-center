package com.grupodot.shoppingcenter.service.impl;

import com.grupodot.shoppingcenter.dao.UserDao;
import com.grupodot.shoppingcenter.dao.builder.UserSearchQueryBuilder;
import com.grupodot.shoppingcenter.dto.User;
import com.grupodot.shoppingcenter.dto.seacrh.user.SearchUserQueryParameters;
import com.grupodot.shoppingcenter.dto.seacrh.user.SearchUserRequest;
import com.grupodot.shoppingcenter.entity.UserEntity;
import com.grupodot.shoppingcenter.enums.DocumentType;
import com.grupodot.shoppingcenter.mapper.UserMapper;
import com.grupodot.shoppingcenter.service.UserService;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserDao dao;

  @Autowired
  private UserMapper mapper;

  @Autowired
  private UserSearchQueryBuilder userSearchQueryBuilder;

  @Autowired
  private BCryptPasswordEncoder bCryptPasswordEncoder;

  @Override
  @Transactional
  public Long create(User user) {
    user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    UserEntity entity = mapper.toUserEntity(user);
    validateIdNumberAndDocumentType(user.getDocumentType(), user.getIdNumber());
    validateUniqueEmail(user.getEmail());

    return dao.save(entity)
        .getId();
  }

  @Override
  @Transactional
  public void update(User user) {
    validateEntityExists(user.getId());
    validateUniqueEmail(user.getEmail());
    validateIdNumberAndDocumentType(user.getDocumentType(), user.getIdNumber());

    UserEntity entity = mapper.toUserEntity(user);
    entity.setPassword(bCryptPasswordEncoder.encode(entity.getPassword()));
    dao.save(entity);
  }

  @Override
  @Transactional(readOnly = true)
  public User getById(Long id) {
    Optional<UserEntity> entityOpt = dao.findById(id);
    if (entityOpt.isPresent()) {
      User savedUser = mapper.toUser(entityOpt.get());
      return savedUser;
    } else {
      throw new EntityNotFoundException("User not found");
    }
  }

  @Override
  public void delete(Long id) {
    validateEntityExists(id);

    UserEntity entity = mapper.toUserEntity(getById(id));
    dao.delete(entity);
  }

  @Override
  public Page<User> search(SearchUserRequest params) {
    Pageable pageable = params.createPageRequest();
    Page<UserEntity> entities = dao.findAll(userSearchQueryBuilder.whereClauseFromSearchParameters(params), pageable);

    Page<User> page = entities.map(entity -> mapper.toUser(entity));
    return page;
  }

  public void validateIdNumberAndDocumentType(DocumentType documentType, String idNumber) {
    SearchUserQueryParameters searchUserQueryParameters = new SearchUserQueryParameters();
    searchUserQueryParameters.setDocumentType(documentType);
    searchUserQueryParameters.setIdNumber(idNumber);

    SearchUserRequest params = new SearchUserRequest();
    params.setPage(0);
    params.setSize(1);
    params.setSearch(searchUserQueryParameters);

    if (this.search(params)
        .getNumberOfElements() > 0) {
      throw new ValidationException("User already registered");
    }
  }

  public void validateUniqueEmail(String email) {
    SearchUserQueryParameters searchUserQueryParameters = new SearchUserQueryParameters();
    searchUserQueryParameters.setEmail(email);

    SearchUserRequest params = new SearchUserRequest();
    params.setPage(0);
    params.setSize(1);
    params.setSearch(searchUserQueryParameters);

    if (this.search(params)
        .getNumberOfElements() > 0) {
      throw new ValidationException("Email already registered");
    }
  }

  public void validateEntityExists(Long id) {
    if (id == null || getById(id) == null) {
      throw new EntityNotFoundException("User not found");
    }
  }
}
