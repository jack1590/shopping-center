package com.grupodot.shoppingcenter.service.impl;

import com.grupodot.shoppingcenter.dao.ProductDao;
import com.grupodot.shoppingcenter.dao.builder.ProductSearchQueryBuilder;
import com.grupodot.shoppingcenter.dto.Product;
import com.grupodot.shoppingcenter.dto.seacrh.product.SearchProductQueryParameters;
import com.grupodot.shoppingcenter.dto.seacrh.product.SearchProductRequest;
import com.grupodot.shoppingcenter.entity.ProductEntity;
import com.grupodot.shoppingcenter.mapper.ProductMapper;
import com.grupodot.shoppingcenter.service.ProductService;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductServiceImpl implements ProductService {

  @Autowired
  private ProductDao dao;

  @Autowired
  private ProductMapper mapper;

  @Autowired
  private ProductSearchQueryBuilder productSearchQueryBuilder;

  @Override
  @Transactional
  public Long create(Product product) {
    ProductEntity entity = mapper.toProductEntity(product);
    validateUniqueCode(product.getCode());

    return dao.save(entity)
        .getId();
  }

  @Override
  @Transactional
  public void update(Product product) {
    validateEntityExists(product.getId());
    validateUniqueCode(product.getCode());
    ProductEntity entity = mapper.toProductEntity(product);
    dao.save(entity);
  }

  @Override
  @Transactional(readOnly = true)
  public Product getById(Long id) {
    Optional<ProductEntity> entityOpt = dao.findById(id);
    if (entityOpt.isPresent()) {
      Product savedProduct = mapper.toProduct(entityOpt.get());
      return savedProduct;
    } else {
      throw new EntityNotFoundException("Product not found");
    }
  }

  @Override
  @Transactional
  public void delete(Long productId) {
    ProductEntity entity = mapper.toProductEntity(getById(productId));
    dao.delete(entity);
  }

  @Override
  public Page<Product> search(SearchProductRequest params) {
    Pageable pageable = params.createPageRequest();
    Page<ProductEntity> entities = dao.findAll(productSearchQueryBuilder.whereClauseFromSearchParameters(params), pageable);

    Page<Product> page = entities.map(entity -> mapper.toProduct(entity));
    return page;
  }

  public void validateUniqueCode(String code) {
    SearchProductQueryParameters searchProductQueryParameters = new SearchProductQueryParameters();
    searchProductQueryParameters.setCode(code);

    SearchProductRequest params = new SearchProductRequest();
    params.setPage(0);
    params.setSize(1);
    params.setSearch(searchProductQueryParameters);

    if (this.search(params)
        .getNumberOfElements() > 0) {
      throw new ValidationException("Code already registered");
    }
  }

  public void validateEntityExists(Long id) {
    if (id == null || getById(id) == null) {
      throw new EntityNotFoundException("Product not found");
    }
  }
}
