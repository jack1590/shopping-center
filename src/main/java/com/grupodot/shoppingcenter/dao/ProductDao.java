package com.grupodot.shoppingcenter.dao;

import com.grupodot.shoppingcenter.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDao extends JpaRepository<ProductEntity, Long>, QuerydslPredicateExecutor<ProductEntity> {}
