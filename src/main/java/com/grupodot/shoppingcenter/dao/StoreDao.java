package com.grupodot.shoppingcenter.dao;

import com.grupodot.shoppingcenter.entity.StoreEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreDao extends JpaRepository<StoreEntity, Long>, QuerydslPredicateExecutor<StoreEntity> {}
