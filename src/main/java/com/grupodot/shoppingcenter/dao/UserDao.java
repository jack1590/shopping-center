package com.grupodot.shoppingcenter.dao;

import com.grupodot.shoppingcenter.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends JpaRepository<UserEntity, Long>, QuerydslPredicateExecutor<UserEntity> {

}
