package com.grupodot.shoppingcenter.dao;

import com.grupodot.shoppingcenter.entity.ProductStoreEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductStoreDao extends JpaRepository<ProductStoreEntity, Long> {}
