package com.grupodot.shoppingcenter.dao.builder;

import com.grupodot.shoppingcenter.dao.builder.util.OptionalBooleanBuilder;
import com.grupodot.shoppingcenter.dto.seacrh.product.SearchProductQueryParameters;
import com.grupodot.shoppingcenter.dto.seacrh.product.SearchProductRequest;
import com.grupodot.shoppingcenter.entity.QProductEntity;
import com.querydsl.core.types.Predicate;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ProductSearchQueryBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(ProductSearchQueryBuilder.class);

  private QProductEntity columns = QProductEntity.productEntity;

  public Predicate whereClauseFromSearchParameters(SearchProductRequest params) {
    OptionalBooleanBuilder expression = new OptionalBooleanBuilder();

    includeSearchValues(params.getSearch(), expression);
    Predicate value = expression.getValue();

    LOG.info("Generated Query {}", value);
    return value;
  }

  private void includeSearchValues(SearchProductQueryParameters search, OptionalBooleanBuilder expression) {

    if (Objects.nonNull(search)) {
      expression.notNullAnd(columns.code::equalsIgnoreCase, search.getCode());
      expression.notNullAnd(columns.category.name::containsIgnoreCase, search.getCategoryName());
      expression.notNullAnd(columns.name::containsIgnoreCase, search.getName());
    }

  }

}
