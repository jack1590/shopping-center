package com.grupodot.shoppingcenter.dao.builder;

import com.grupodot.shoppingcenter.dao.builder.util.OptionalBooleanBuilder;
import com.grupodot.shoppingcenter.dto.seacrh.user.SearchUserQueryParameters;
import com.grupodot.shoppingcenter.dto.seacrh.user.SearchUserRequest;
import com.grupodot.shoppingcenter.entity.QUserEntity;
import com.querydsl.core.types.Predicate;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class UserSearchQueryBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(UserSearchQueryBuilder.class);

  private QUserEntity columns = QUserEntity.userEntity;

  public Predicate whereClauseFromSearchParameters(SearchUserRequest params) {
    OptionalBooleanBuilder expression = new OptionalBooleanBuilder();

    includeSearchValues(params.getSearch(), expression);
    Predicate value = expression.getValue();

    LOG.info("Generated Query {}", value);
    return value;
  }

  private void includeSearchValues(SearchUserQueryParameters search, OptionalBooleanBuilder expression) {

    if (Objects.nonNull(search)) {
      expression.notNullAnd(columns.idNumber::equalsIgnoreCase, search.getIdNumber());
      expression.notNullAnd(columns.fullName::containsIgnoreCase, search.getFullName());
      expression.notNullAnd(columns.documentType::eq, search.getDocumentType());
      expression.notNullAnd(columns.email::equalsIgnoreCase, search.getEmail());
    }

  }
}
