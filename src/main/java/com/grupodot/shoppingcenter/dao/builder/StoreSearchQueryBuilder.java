package com.grupodot.shoppingcenter.dao.builder;

import com.grupodot.shoppingcenter.dao.builder.util.OptionalBooleanBuilder;
import com.grupodot.shoppingcenter.dto.seacrh.store.SearchStoreQueryParameters;
import com.grupodot.shoppingcenter.dto.seacrh.store.SearchStoreRequest;
import com.grupodot.shoppingcenter.entity.QStoreEntity;
import com.querydsl.core.types.Predicate;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class StoreSearchQueryBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(StoreSearchQueryBuilder.class);

  private QStoreEntity columns = QStoreEntity.storeEntity;

  public Predicate whereClauseFromSearchParameters(SearchStoreRequest params) {
    OptionalBooleanBuilder expression = new OptionalBooleanBuilder();

    includeSearchValues(params.getSearch(), expression);
    Predicate value = expression.getValue();

    LOG.info("Generated Query {}", value);
    return value;
  }

  private void includeSearchValues(SearchStoreQueryParameters search, OptionalBooleanBuilder expression) {

    if (Objects.nonNull(search)) {
      expression.notNullAnd(columns.code::equalsIgnoreCase, search.getCode());
      expression.notNullAnd(columns.city.name::containsIgnoreCase, search.getCityName());
      expression.notNullAnd(columns.mobileNumber::containsIgnoreCase, search.getMobileNumber());
      expression.notNullAnd(columns.address::containsIgnoreCase, search.getAddress());
      expression.notNullAnd(columns.zipCode::equalsIgnoreCase, search.getZipCode());
    }

  }
}
