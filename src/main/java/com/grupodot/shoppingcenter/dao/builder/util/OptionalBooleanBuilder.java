/*-
 * This software is the property of:
 *
 * World Fuel Services Corporation.
 * Copyright (c) 2017 World Fuel Services Corporation.
 *
 * It may not be copied, distributed or modified, in part or in whole,
 * by any means whatsoever, without the explicit written permission of World Fuel Services Corporation.
 */
package com.grupodot.shoppingcenter.dao.builder.util;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import java.util.function.Function;


public class OptionalBooleanBuilder {

  private BooleanBuilder booleanBuilder = new BooleanBuilder();

  public <T> OptionalBooleanBuilder notNullAnd(Function<T, BooleanExpression> expressionFunction, T value) {
    if (value != null) {
      booleanBuilder.and(expressionFunction.apply(value));
      return this;
    }
    return this;
  }

  public <T> OptionalBooleanBuilder notNullOr(Function<T, BooleanExpression> expressionFunction, T value) {
    if (value != null) {
      booleanBuilder.or(expressionFunction.apply(value));
    }
    return this;
  }

  public <T> OptionalBooleanBuilder notNullAndLike(Function<String, BooleanExpression> expressionFunction, String value) {
    if (value != null) {
      booleanBuilder.and(expressionFunction.apply(value + "%"));
    }
    return this;
  }

  public Predicate getValue() {
    return booleanBuilder.getValue();
  }

}