package com.grupodot.shoppingcenter.enums;

public enum Role {

  ADMIN("Administrator"),
  CUSTOMER("Customer");

  private String value;

  Role(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
