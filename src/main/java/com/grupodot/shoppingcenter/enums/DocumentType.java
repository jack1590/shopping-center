package com.grupodot.shoppingcenter.enums;

public enum DocumentType {

  CC("CEDULA CIUDADANIA"),
  CE("CEDULA EXTRANJERIA"),
  PS("PASAPORTE");

  private String value;

  DocumentType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
