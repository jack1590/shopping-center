package com.grupodot.shoppingcenter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.io.Serializable;
import java.time.Instant;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Base implements Serializable {

  @NotNull
  private Integer version = 0;

  @JsonProperty(access = Access.WRITE_ONLY)
  private Instant lastUpdatedOn;

  @JsonProperty(access = Access.WRITE_ONLY)
  private String lastUpdatedBy;
}
