package com.grupodot.shoppingcenter.dto;

import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(of = {"id",
                "name"})
@EqualsAndHashCode(of = {"id"}, callSuper = false)
public class Category extends Base {

  @NotNull
  private Long id;

  private String name;
  private String description;
}
