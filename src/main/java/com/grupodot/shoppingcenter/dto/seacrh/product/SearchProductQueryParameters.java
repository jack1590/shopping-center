package com.grupodot.shoppingcenter.dto.seacrh.product;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SearchProductQueryParameters {

  private String code;
  private String categoryName;
  private String name;

}
