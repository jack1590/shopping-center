package com.grupodot.shoppingcenter.dto.seacrh.store;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SearchStoreQueryParameters {

  private String code;
  private String address;
  private String mobileNumber;
  private String cityName;
  private String zipCode;
}
