package com.grupodot.shoppingcenter.dto.seacrh.user;

import com.grupodot.shoppingcenter.enums.DocumentType;
import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class SearchUserQueryParameters {

  private String idNumber;
  private DocumentType documentType;
  private String fullName;
  private String email;

}
