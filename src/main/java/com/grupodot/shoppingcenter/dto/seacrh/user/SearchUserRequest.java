package com.grupodot.shoppingcenter.dto.seacrh.user;

import com.grupodot.shoppingcenter.dto.seacrh.SortField;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@Data
@ToString
public class SearchUserRequest {

  private SearchUserQueryParameters search;
  private SortField sort;
  private Integer page = 0;
  private Integer size = 50;

  public Pageable createPageRequest() {
    if (null == sort) {
      return PageRequest.of(page, size);
    } else {
      return PageRequest.of(page, size, new Sort(sort.getOrder(), sort.getField()));
    }
  }

}
