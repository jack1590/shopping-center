package com.grupodot.shoppingcenter.dto.seacrh;

import lombok.Data;
import org.springframework.data.domain.Sort;

@Data
public class SortField {

  private Sort.Direction order;
  private String field;
}
