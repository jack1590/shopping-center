package com.grupodot.shoppingcenter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.grupodot.shoppingcenter.enums.DocumentType;
import com.grupodot.shoppingcenter.enums.Role;
import java.util.Set;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(of = {"id",
                "fullName"})
@EqualsAndHashCode(of = {"id"}, callSuper = false)
public class User extends Base {

  private Long id;

  @NotNull
  private DocumentType documentType;

  @NotNull
  private String idNumber;

  @NotNull
  private String fullName;

  @NotNull
  @Min(value = 18, message = "The user must be 18 or more")
  private Integer age;

  private String address;

  @NotNull
  @Email
  private String email;

  @NotNull
  @JsonProperty(access = Access.WRITE_ONLY)
  private String password;

  @NotNull
  private Set<Role> roles;

}
