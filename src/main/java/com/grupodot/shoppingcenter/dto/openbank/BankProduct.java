package com.grupodot.shoppingcenter.dto.openbank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class BankProduct {


  @JsonProperty(value = "Sub-category")
  private String subCategory;

  @JsonProperty(value = "Landing-page")
  private String landingPage;
  private String name;

  @JsonProperty(value = "Category")
  private String category;

}
