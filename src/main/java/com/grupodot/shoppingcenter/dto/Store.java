package com.grupodot.shoppingcenter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.util.Set;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(of = {"id",
                "code"})
@EqualsAndHashCode(of = {"id"}, callSuper = false)
public class Store extends Base {

  private Long id;

  @NotNull
  private String code;

  @NotNull
  private String address;

  @NotNull
  private String mobileNumber;

  @NotNull
  private City city;

  @NotNull
  private String zipCode;

  @JsonProperty(access = Access.READ_ONLY)
  private Set<Product> products;

}
