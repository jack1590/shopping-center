package com.grupodot.shoppingcenter.dto;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Error implements Serializable {

  private String correlationId;
  private String message;
  private List<String> errors;


}
