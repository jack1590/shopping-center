package com.grupodot.shoppingcenter.dto;

import java.util.Set;
import javax.validation.Valid;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ValidProductStore {

  @Valid
  private Set<ProductStore> productStores;
}
