package com.grupodot.shoppingcenter.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(of = {"id",
                "name"})
@EqualsAndHashCode(of = {"id"}, callSuper = false)
public class City extends Base {

  private Long id;

  private String name;
  private String displayName;
}
