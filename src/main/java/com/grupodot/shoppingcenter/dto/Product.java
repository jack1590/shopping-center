package com.grupodot.shoppingcenter.dto;

import java.math.BigDecimal;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(of = {"code",
                "name"})
@EqualsAndHashCode(of = {"id"}, callSuper = false)
public class Product extends Base {

  private Long id;

  @NotNull
  private String code;

  @NotNull
  private Category category;

  @NotEmpty
  private String name;

  @NotEmpty
  private String description;

  private BigDecimal price;

  private Integer stock;
}
