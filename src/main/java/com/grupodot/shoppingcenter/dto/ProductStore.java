package com.grupodot.shoppingcenter.dto;

import java.math.BigDecimal;
import java.time.Instant;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"storeId",
                         "productId"})
public class ProductStore {

  @NotNull
  private Long storeId;

  @NotNull
  private Long productId;

  @NotNull
  private Integer stock;

  @NotNull
  private BigDecimal price;

  private Integer version = 0;
  private Instant lastUpdatedOn;
  private String lastUpdatedBy;

}
