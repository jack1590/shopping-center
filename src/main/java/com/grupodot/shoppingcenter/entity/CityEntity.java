package com.grupodot.shoppingcenter.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "city")
@Table(name = "city")
@Getter
@Setter
@ToString(of = {"name"})
@EqualsAndHashCode(callSuper = true)
public class CityEntity extends BaseEntity {

  private String name;
  private String displayName;
}
