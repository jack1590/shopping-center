package com.grupodot.shoppingcenter.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "product")
@Table(name = "product")
@Getter
@Setter
@ToString(of = {"code",
                "name"})
@EqualsAndHashCode(callSuper = true)
public class ProductEntity extends BaseEntity {

  private String code;

  @ManyToOne(fetch = FetchType.EAGER)
  private CategoryEntity category;

  private String name;
  private String description;
}
