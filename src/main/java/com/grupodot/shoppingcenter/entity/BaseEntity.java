package com.grupodot.shoppingcenter.entity;

import com.grupodot.shoppingcenter.config.UserContext;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"id",
                         "version"})
@MappedSuperclass
public class BaseEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Version
  private Integer version;

  private Instant lastUpdatedOn;
  private String lastUpdatedBy;

  @PrePersist
  public void beforeCreate() {
    version = 0;
    lastUpdatedOn = Instant.now();
    lastUpdatedBy = UserContext.getUser();
  }

  @PreUpdate
  public void beforeUpdate() {
    lastUpdatedOn = Instant.now();
    lastUpdatedBy = UserContext.getUser();
  }
}