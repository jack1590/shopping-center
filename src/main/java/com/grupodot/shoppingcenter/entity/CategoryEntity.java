package com.grupodot.shoppingcenter.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "category")
@Table(name = "category")
@Getter
@Setter
@ToString(of = {"name",
                "description"})
@EqualsAndHashCode(callSuper = true)
public class CategoryEntity extends BaseEntity {

  private String name;
  private String description;
}
