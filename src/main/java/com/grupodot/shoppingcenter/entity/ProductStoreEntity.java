package com.grupodot.shoppingcenter.entity;

import com.grupodot.shoppingcenter.config.UserContext;
import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "store_product")
@Table(name = "store_product")
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "entityId")
public class ProductStoreEntity {

  @EmbeddedId
  private ProductStoreEntityId entityId;

  @ManyToOne(fetch = FetchType.EAGER)
  @MapsId("productId")
  private ProductEntity product;

  private Integer stock;
  private BigDecimal price;

  @Version
  private Integer version;

  private Instant lastUpdatedOn;
  private String lastUpdatedBy;

  @PrePersist
  public void beforeCreate() {
    version = 0;
    lastUpdatedOn = Instant.now();
    lastUpdatedBy = UserContext.getUser();
  }

  @PreUpdate
  public void beforeUpdate() {
    lastUpdatedOn = Instant.now();
    lastUpdatedBy = UserContext.getUser();
  }

}
