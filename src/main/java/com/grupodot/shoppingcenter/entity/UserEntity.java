package com.grupodot.shoppingcenter.entity;

import com.grupodot.shoppingcenter.enums.DocumentType;
import com.grupodot.shoppingcenter.enums.Role;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "user")
@Table(name = "user")
@Getter
@Setter
@ToString(of = {"idNumber",
                "fullName"})
@EqualsAndHashCode(callSuper = true)
public class UserEntity extends BaseEntity {


  @Enumerated(EnumType.STRING)
  private DocumentType documentType;

  private String idNumber;
  private String fullName;
  private Integer age;
  private String address;
  private String email;
  private String password;

  @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
  @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
  @Enumerated(EnumType.STRING)
  @Column(name = "role")
  private Set<Role> roles;


}
