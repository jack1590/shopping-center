package com.grupodot.shoppingcenter.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Embeddable
public class ProductStoreEntityId implements Serializable {

  @Column(name = "store_id")
  private Long storeId;

  @Column(name = "product_id")
  private Long productId;
}