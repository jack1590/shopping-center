package com.grupodot.shoppingcenter.entity;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "store")
@Table(name = "store")
@Getter
@Setter
@ToString(of = {"code"})
@EqualsAndHashCode(callSuper = true)
public class StoreEntity extends BaseEntity {

  private String code;
  private String address;
  private String mobileNumber;

  @ManyToOne(fetch = FetchType.EAGER)
  private CityEntity city;

  private String zipCode;

  /**
   * @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST})
   * @JoinTable(name = "store_product", joinColumns = @JoinColumn(name = "store_id"), inverseJoinColumns = @JoinColumn(name = "product_id")) private
   * Set<ProductEntity> products;
   **/

  @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<ProductStoreEntity> products;
}
