package com.grupodot.shoppingcenter.mapper;

import com.grupodot.shoppingcenter.dto.User;
import com.grupodot.shoppingcenter.entity.UserEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class UserMapper {

  public abstract UserEntity toUserEntity(User user);

  public abstract User toUser(UserEntity userEntity);

}
