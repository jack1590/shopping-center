package com.grupodot.shoppingcenter.mapper;

import com.grupodot.shoppingcenter.dto.ProductStore;
import com.grupodot.shoppingcenter.entity.ProductStoreEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class ProductStoreMapper {

  @Mapping(target = "entityId.storeId", source = "storeId")
  @Mapping(target = "entityId.productId", source = "productId")
  public abstract ProductStoreEntity toProductStoreEntity(ProductStore product);

}
