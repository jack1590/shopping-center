package com.grupodot.shoppingcenter.mapper;

import com.grupodot.shoppingcenter.dto.Product;
import com.grupodot.shoppingcenter.dto.Store;
import com.grupodot.shoppingcenter.entity.ProductStoreEntity;
import com.grupodot.shoppingcenter.entity.StoreEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class StoreMapper {

  public abstract StoreEntity toStoreEntity(Store store);

  public abstract Store toStore(StoreEntity storeEntity);

  @Mapping(target = "id", source = "product.id")
  @Mapping(target = "code", source = "product.code")
  @Mapping(target = "category", source = "product.category")
  @Mapping(target = "name", source = "product.name")
  @Mapping(target = "description", source = "product.description")
  public abstract Product productStoreEntityToProduct(ProductStoreEntity productStoreEntity);

}
