package com.grupodot.shoppingcenter.mapper;

import com.grupodot.shoppingcenter.dto.Product;
import com.grupodot.shoppingcenter.entity.ProductEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class ProductMapper {

  public abstract ProductEntity toProductEntity(Product product);

  public abstract Product toProduct(ProductEntity productEntity);
}
