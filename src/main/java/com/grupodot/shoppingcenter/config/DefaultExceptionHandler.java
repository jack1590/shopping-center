package com.grupodot.shoppingcenter.config;

import com.grupodot.shoppingcenter.dto.Error;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class DefaultExceptionHandler {

  @ExceptionHandler(EntityNotFoundException.class)
  public final ResponseEntity<Error> handleException(HttpServletRequest request, EntityNotFoundException ex) {
    Error exceptionResponse = new Error(RequestContext.getCorrelationId(), ex.getMessage(), null);
    return new ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Error> invalidInput(MethodArgumentNotValidException ex) {
    BindingResult result = ex.getBindingResult();
    Error response = new Error(RequestContext.getCorrelationId(), "Invalid inputs.", fromBindingErrors(result));
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler({ValidationException.class})
  public ResponseEntity<Error> handleException(ValidationException e) {

    Error error = new Error(RequestContext.getCorrelationId(), e.getMessage(), null);

    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler({ObjectOptimisticLockingFailureException.class})
  public ResponseEntity<Error> handleException(ObjectOptimisticLockingFailureException e) {

    Error error = new Error(RequestContext.getCorrelationId(), "Resource was updated or deleted by another transaction", null);

    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }


  private List<String> fromBindingErrors(BindingResult result) {
    List<String> validErrors = new ArrayList<>();
    for (FieldError fieldError : result.getFieldErrors()) {
      validErrors.add(fieldError.getField() + " - " + fieldError.getDefaultMessage());
    }
    return validErrors;
  }

}
