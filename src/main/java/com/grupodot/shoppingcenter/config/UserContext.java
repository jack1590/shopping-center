/*-
 * This software is the property of:
 *
 * World Fuel Services Corporation.
 * Copyright (c) 2017 World Fuel Services Corporation.
 *
 * It may not be copied, distributed or modified, in part or in whole,
 * by any means whatsoever, without the explicit written permission of World Fuel Services Corporation.
 */
package com.grupodot.shoppingcenter.config;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserContext {

  private static final String ANONYMOUS = "anonymous";
  private final String idNumber;
  private final List<GrantedAuthority> authorities;

  public UserContext(String idNumber, List<GrantedAuthority> authorities) {
    this.idNumber = idNumber;
    this.authorities = authorities;
  }

  public static UserContext create(String idNumber, List<GrantedAuthority> authorities) {
    if (StringUtils.isBlank(idNumber)) {
      throw new IllegalArgumentException("Username is blank: " + idNumber);
    }
    return new UserContext(idNumber, authorities);
  }

  public static String getUser() {
    if (SecurityContextHolder.getContext()
        .getAuthentication()
        .getPrincipal() == null) {
      return ANONYMOUS;
    }
    return ((UserContext) SecurityContextHolder.getContext()
        .getAuthentication()
        .getPrincipal()).idNumber;
  }

  public List<GrantedAuthority> getAuthorities() {
    return authorities;
  }
}

