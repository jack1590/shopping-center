/*-
 * This software is the property of:
 *
 * World Fuel Services Corporation.
 * Copyright (c) 2017 World Fuel Services Corporation.
 *
 * It may not be copied, distributed or modified, in part or in whole,
 * by any means whatsoever, without the explicit written permission of World Fuel Services Corporation.
 */
package com.grupodot.shoppingcenter.config;

import java.io.IOException;
import java.util.UUID;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

@Component
public class CorrelationHeaderFilter implements Filter {

  @Override
  public void init(FilterConfig filterConfig) {
    // overriding init method from parent
  }

  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

    final HttpServletRequest httpServletRequest = (HttpServletRequest) req;
    String corrId = httpServletRequest.getHeader(RequestContext.CORRELATION_ID_HEADER);

    if (corrId == null) {
      corrId = UUID.randomUUID()
          .toString();
    }

    HttpServletResponse httpServletResponse = (HttpServletResponse) res;
    HttpServletResponseWrapper httpServletResponseWrapper = new HttpServletResponseWrapper(httpServletResponse);
    httpServletResponseWrapper.setHeader(RequestContext.CORRELATION_ID_HEADER, corrId);

    RequestContext.setCorrelationId(corrId);
    MDC.put(RequestContext.CORRELATION_ID_HEADER, corrId);

    try {
      chain.doFilter(httpServletRequest, httpServletResponseWrapper);
    } finally {
      // Clean Up
      MDC.remove(RequestContext.CORRELATION_ID_HEADER);
      RequestContext.remove();
    }
  }

  @Override
  public void destroy() {
    // overriding destroy method from parent
  }

}
