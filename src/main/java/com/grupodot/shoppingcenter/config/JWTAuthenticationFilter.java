package com.grupodot.shoppingcenter.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.grupodot.shoppingcenter.dto.User;
import com.grupodot.shoppingcenter.security.TokenFactory;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

public class JWTAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

  private SecurityConfig securityConfig;

  private TokenFactory tokenFactory;

  JWTAuthenticationFilter(SecurityConfig securityConfig, TokenFactory tokenFactory, String defaultFilterProcessesUrl) {
    super(defaultFilterProcessesUrl);
    this.securityConfig = securityConfig;
    this.tokenFactory = tokenFactory;
  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException, IOException {
    if (!HttpMethod.POST.name()
        .equals(req.getMethod())) {
      if (logger.isDebugEnabled()) {
        logger.debug("Authentication method not supported. Request method: " + req.getMethod());
      }
      throw new AuthenticationServiceException("Authentication method not supported");
    }

    User creds = new ObjectMapper().readValue(req.getInputStream(), User.class);

    if (StringUtils.isBlank(creds.getIdNumber()) || StringUtils.isBlank(creds.getPassword())) {
      throw new AuthenticationServiceException("Username or Password not provided");
    }

    UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(creds.getIdNumber(), creds.getPassword(), null);
    return getAuthenticationManager().authenticate(token);
  }

  @Override
  protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain, Authentication auth) {

    User user = (User) auth.getPrincipal();

    res.addHeader(securityConfig.getTokenHeader(), securityConfig.getTokenPrefix() + tokenFactory.createAccessJwtToken(user));
  }

}
