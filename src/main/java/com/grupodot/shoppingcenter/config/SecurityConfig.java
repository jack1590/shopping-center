package com.grupodot.shoppingcenter.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Configuration
@Component
@ConfigurationProperties(prefix = "jwt.security")
@Getter
@Setter
public class SecurityConfig {

  private String secrets;
  private Integer expirationTime;
  private String tokenPrefix;
  private String tokenHeader;
  private String header;
  private String authoritiesKey;

  @Bean
  public BCryptPasswordEncoder bCryptPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }

}
