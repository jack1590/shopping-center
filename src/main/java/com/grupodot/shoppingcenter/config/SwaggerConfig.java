package com.grupodot.shoppingcenter.config;


import com.grupodot.shoppingcenter.contants.ApplicationConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Bean
  public Docket bizopsUserApi() {
    return getDocket("1 - Shopping Center Users API", shoppingApiInfo(),
        ApplicationConstants.RESOURCE_VERSION_1 + ApplicationConstants.USER_RESOURCE_NAME + "/**");
  }

  @Bean
  public Docket bizopsStoreApi() {
    return getDocket("2 - Shopping Center Store API", shoppingApiInfo(),
        ApplicationConstants.RESOURCE_VERSION_1 + ApplicationConstants.STORE_RESOURCE_NAME + "/**");
  }

  @Bean
  public Docket bizopsProductApi() {
    return getDocket("3 - Shopping Center Product API", shoppingApiInfo(),
        ApplicationConstants.RESOURCE_VERSION_1 + ApplicationConstants.PRODUCT_RESOURCE_NAME + "/**");
  }

  @Bean
  public Docket actuatorApi() {
    return getDocket("4 - Actuator API", actuatorApiInfo(), "/actuator/**");
  }

  private Docket getDocket(String groupName, ApiInfo apiInfo, String pathSelector) {
    return new Docket(DocumentationType.SWAGGER_2).groupName(groupName)
        .apiInfo(apiInfo)
        .select()
        .paths(PathSelectors.ant(pathSelector))
        .build()
        .forCodeGeneration(true);
  }

  private ApiInfo shoppingApiInfo() {
    Contact catapultTeamContact = new Contact("Juan Joya", null, "jcarl.30@gmail.com");

    return new ApiInfoBuilder().title("GrupoDot - Business Operations API")
        .contact(catapultTeamContact)
        .build();
  }

  private ApiInfo actuatorApiInfo() {
    Contact catapultTeamContact = new Contact("Juan Joya", null, "jcarl.30@gmail.com");

    return new ApiInfoBuilder().title("Actuator API")
        .contact(catapultTeamContact)
        .build();
  }
}
