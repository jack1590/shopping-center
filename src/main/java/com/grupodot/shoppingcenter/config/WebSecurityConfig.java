package com.grupodot.shoppingcenter.config;

import com.grupodot.shoppingcenter.contants.ApplicationConstants;
import com.grupodot.shoppingcenter.security.JwtAuthenticationProvider;
import com.grupodot.shoppingcenter.security.JwtAuthorizationProvider;
import com.grupodot.shoppingcenter.security.SkipPathMatcher;
import com.grupodot.shoppingcenter.security.TokenFactory;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@ConditionalOnProperty(value = "security.enabled", havingValue = "true")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private JwtAuthorizationProvider jwtAuthorizationProvider;

  @Autowired
  private JwtAuthenticationProvider jwtAuthenticationProvider;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private SecurityConfig securityConfig;

  @Autowired
  private TokenFactory tokenFactory;

  protected JWTAuthenticationFilter buildAuthenticationFilter(String loginEntryPoint) throws Exception {
    JWTAuthenticationFilter filter = new JWTAuthenticationFilter(securityConfig, tokenFactory, loginEntryPoint);
    filter.setAuthenticationManager(this.authenticationManager);
    return filter;
  }

  protected JWTAuthorizationFilter buildAuthorizationFilter(List<String> pathsToSkip, String pattern) throws Exception {
    SkipPathMatcher matcher = new SkipPathMatcher(pathsToSkip, pattern);
    JWTAuthorizationFilter filter = new JWTAuthorizationFilter(securityConfig, matcher);
    filter.setAuthenticationManager(this.authenticationManager);
    return filter;
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) {
    auth.authenticationProvider(jwtAuthorizationProvider);
    auth.authenticationProvider(jwtAuthenticationProvider);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    List<String> permitAllEndpointList = Arrays.asList(ApplicationConstants.AUTHENTICATION_URL);

    http.csrf()
        .disable() // We don't need CSRF for JWT based authentication
        .exceptionHandling()
        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        .and()
        .authorizeRequests()
        .antMatchers(permitAllEndpointList.toArray(new String[permitAllEndpointList.size()]))
        .permitAll()
        .and()
        .authorizeRequests()
        .antMatchers(ApplicationConstants.API_ROOT_URL)
        .authenticated() // Protected API End-points
        .and()
        .addFilterBefore(customCorsFilter(), UsernamePasswordAuthenticationFilter.class)
        .addFilterBefore(buildAuthenticationFilter(ApplicationConstants.AUTHENTICATION_URL), UsernamePasswordAuthenticationFilter.class)
        .addFilterBefore(buildAuthorizationFilter(permitAllEndpointList, ApplicationConstants.API_ROOT_URL),
            UsernamePasswordAuthenticationFilter.class);
  }

  private CorsFilter customCorsFilter() {
    CorsConfiguration config = new CorsConfiguration();
    config.setAllowCredentials(true);
    config.addAllowedOrigin("*");
    config.addAllowedHeader("*");
    config.setMaxAge(36000L);
    config.setAllowedMethods(Arrays.asList("GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"));
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/api/**", config);
    return new CorsFilter(source);
  }

}
