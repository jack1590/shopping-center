/*-
 * This software is the property of:
 *
 * World Fuel Services Corporation.
 * Copyright (c) 2017 World Fuel Services Corporation.
 *
 * It may not be copied, distributed or modified, in part or in whole,
 * by any means whatsoever, without the explicit written permission of World Fuel Services Corporation.
 */
package com.grupodot.shoppingcenter.config;

public class RequestContext {

  public static final String CORRELATION_ID_HEADER = "x-catapult-correlation-id";

  private static final ThreadLocal<String> CORRELATION_ID = new ThreadLocal<>();


  public static String getCorrelationId() {
    return CORRELATION_ID.get();
  }

  public static void setCorrelationId(String correlationId) {
    CORRELATION_ID.set(correlationId);
  }

  public static void remove() {
    CORRELATION_ID.remove();
  }

}
