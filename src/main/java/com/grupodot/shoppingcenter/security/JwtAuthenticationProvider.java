package com.grupodot.shoppingcenter.security;

import com.grupodot.shoppingcenter.dto.User;
import com.grupodot.shoppingcenter.dto.seacrh.user.SearchUserQueryParameters;
import com.grupodot.shoppingcenter.dto.seacrh.user.SearchUserRequest;
import com.grupodot.shoppingcenter.service.UserService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

  @Autowired
  private BCryptPasswordEncoder encoder;

  @Autowired
  private UserService userService;


  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String idNumber = (String) authentication.getPrincipal();
    String password = (String) authentication.getCredentials();

    SearchUserQueryParameters searchUserQueryParameters = new SearchUserQueryParameters();
    searchUserQueryParameters.setIdNumber(idNumber);
    SearchUserRequest searchUserRequest = new SearchUserRequest();
    searchUserRequest.setSearch(searchUserQueryParameters);

    Page<User> pageUser = userService.search(searchUserRequest);

    if (pageUser.getNumberOfElements() == 0) {
      throw new UsernameNotFoundException("User not found");
    }

    User user = pageUser.getContent()
        .get(0);

    if (!encoder.matches(password, user.getPassword())) {
      throw new BadCredentialsException("Authentication Failed. Username or Password not valid.");
    }

    if (user.getRoles() == null) {
      throw new InsufficientAuthenticationException("User has no roles assigned");
    }

    List<GrantedAuthority> authorities = user.getRoles()
        .stream()
        .map(authority -> new SimpleGrantedAuthority(authority.name()))
        .collect(Collectors.toList());

    return new UsernamePasswordAuthenticationToken(user, null, authorities);
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
  }

}