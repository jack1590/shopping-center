package com.grupodot.shoppingcenter.security;

import com.grupodot.shoppingcenter.config.UserContext;
import java.util.Collection;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class JwtAuthenticationToken extends AbstractAuthenticationToken {

  private String token;
  private UserContext principal;

  public JwtAuthenticationToken(String token, Collection<? extends GrantedAuthority> authorities) {
    super(authorities);
    super.setAuthenticated(true);
    this.token = token;
  }

  JwtAuthenticationToken(UserContext user, String token, Collection<? extends GrantedAuthority> authorities) {
    super(authorities);
    super.setAuthenticated(true);
    this.token = token;
    this.principal = user;
  }

  @Override
  public Object getCredentials() {
    return token;
  }

  @Override
  public Object getPrincipal() {
    return this.principal;
  }

  @Override
  public void eraseCredentials() {
    super.eraseCredentials();
    this.token = null;
  }
}
