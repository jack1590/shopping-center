package com.grupodot.shoppingcenter.security;

import com.grupodot.shoppingcenter.config.SecurityConfig;
import com.grupodot.shoppingcenter.config.UserContext;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthorizationProvider implements AuthenticationProvider {

  @Autowired
  private SecurityConfig securityConfig;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {

    String token = authentication.getCredentials()
        .toString();

    Jws<Claims> jwsClaims = Jwts.parser()
        .setSigningKey(securityConfig.getSecrets())
        .parseClaimsJws(token.replace(securityConfig.getTokenPrefix(), ""));

    String subject = jwsClaims.getBody()
        .getSubject();
    List<String> scopes = jwsClaims.getBody()
        .get(securityConfig.getAuthoritiesKey(), List.class);

    List<GrantedAuthority> authorities = scopes.stream()
        .map(authority -> new SimpleGrantedAuthority("ROLE_" + authority))
        .collect(Collectors.toList());

    UserContext userContext = UserContext.create(subject, authorities);

    return new JwtAuthenticationToken(userContext, subject, authorities);
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
  }
}
