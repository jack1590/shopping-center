package com.grupodot.shoppingcenter.security;

import com.grupodot.shoppingcenter.config.SecurityConfig;
import com.grupodot.shoppingcenter.dto.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TokenFactory {

  @Autowired
  private SecurityConfig securityConfig;

  public String createAccessJwtToken(User user) {
    if (StringUtils.isBlank(user.getIdNumber())) {
      throw new IllegalArgumentException("Cannot create JWT Token without username");
    }

    if (user.getRoles()
        .isEmpty()) {
      throw new IllegalArgumentException("User doesn't have any privileges");
    }

    Claims claims = Jwts.claims()
        .setSubject(user.getIdNumber());
    claims.put(securityConfig.getAuthoritiesKey(), user.getRoles()
        .stream()
        .map(Enum::name)
        .collect(Collectors.toList()));

    return Jwts.builder()
        .setClaims(claims)
        .setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + this.securityConfig.getExpirationTime()))
        .signWith(SignatureAlgorithm.HS512, securityConfig.getSecrets())
        .compact();
  }

}
