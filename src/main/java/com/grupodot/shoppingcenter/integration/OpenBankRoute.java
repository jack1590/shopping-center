package com.grupodot.shoppingcenter.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.grupodot.shoppingcenter.dto.openbank.BankProduct;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class OpenBankRoute extends RouteBuilder {

  @Override
  public void configure() throws Exception {

    from("direct:getRestFromOpenBank").to("https://apis-bank-test.apigee.net/apis/v2.0.1/products")
        .process(new Processor() {
          public void process(Exchange exchange) throws Exception {

            String jsonString = exchange.getIn()
                .getBody(String.class);

            ObjectMapper mapper = new ObjectMapper();

            JSONObject jsonObject = new JSONObject(jsonString);
            List<BankProduct> bankProduct = mapper.readValue(jsonObject.getJSONObject("Data")
                .getJSONArray("BankProducts")
                .toString(), new TypeReference<List<BankProduct>>() {});

            exchange.getOut()
                .setBody(bankProduct.stream()
                    .collect(Collectors.groupingBy(BankProduct::getCategory)));

          }
        });
  }
}
