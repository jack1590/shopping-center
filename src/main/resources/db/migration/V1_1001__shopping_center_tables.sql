CREATE TABLE category (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  description varchar(255) NOT NULL,
  last_updated_by varchar(255) NOT NULL,
  last_updated_on datetime NOT NULL,
  version int(11) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE city (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  display_name varchar(255) NOT NULL,
  last_updated_by varchar(255) NOT NULL,
  last_updated_on datetime NOT NULL,
  version int(11) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE product (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  code varchar(255) NOT NULL,
  name varchar(255) NOT NULL,
  description varchar(255) NOT NULL,
  category_id bigint(20) NOT NULL,
  last_updated_by varchar(255) NOT NULL,
  last_updated_on datetime NOT NULL,
  version int(11) NOT NULL,
  PRIMARY KEY (id)
) ;

CREATE TABLE store (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  code varchar(255) NOT NULL,
  address varchar(255) NOT NULL,
  mobile_number varchar(255) NOT NULL,
  zip_code varchar(255) DEFAULT NULL,
  city_id bigint(20) NOT NULL,
  last_updated_by varchar(255) NOT NULL,
  last_updated_on datetime NOT NULL,
  version int(11) NOT NULL,
  PRIMARY KEY (id)
) ;

CREATE TABLE user (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  address varchar(255) DEFAULT NULL,
  age int(3)  NOT NULL,
  document_type varchar(255) NOT NULL,
  email varchar(255)  NOT NULL,
  full_name varchar(255)  NOT NULL,
  id_number varchar(255)  NOT NULL,
  password varchar(255)  NOT NULL,
  last_updated_by varchar(255)  NOT NULL,
  last_updated_on datetime  NOT NULL,
  version int(11)  NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE `store_product` (
  `store_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  price decimal NOT NULL,
  stock int(11) NOT NULL,
  last_updated_by varchar(255)  NOT NULL,
  last_updated_on datetime  NOT NULL,
  version int(11)  NOT NULL,
  PRIMARY KEY (store_id,product_id),
  INDEX store_products_index (product_id, store_id)
);

CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `role` varchar(255) NOT NULL DEFAULT "CUSTOMER",
  PRIMARY KEY (user_id,role)
);