-- Foreign Key Constraints
alter table user_role add constraint fk_user_role_user foreign key (user_id) references user (id);
alter table product add constraint fk_product_category foreign key (category_id) references category (id);
alter table store add constraint fk_store_city foreign key (city_id) references city (id);
alter table store_product add constraint fk_store_product_store foreign key (store_id) references store (id);
alter table store_product add constraint fk_store_product_product foreign key (product_id) references product (id);

-- Unique keys
alter table user add constraint uq_document unique key (document_type,id_number);
alter table user add constraint uq_email unique key (email);

alter table store add constraint uq_code unique key (code);

alter table product add constraint uq_code unique key (code);