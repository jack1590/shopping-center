package com.grupodot.shoppingcenter.service.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import com.grupodot.shoppingcenter.dao.StoreDao;
import com.grupodot.shoppingcenter.dto.Store;
import com.grupodot.shoppingcenter.entity.StoreEntity;
import com.grupodot.shoppingcenter.mapper.StoreMapper;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.data.domain.Page;

public class StoreServiceImplTest {

  @InjectMocks
  @Spy
  private StoreServiceImpl storeService;

  @Mock
  private StoreDao dao;

  @Mock
  private StoreMapper mapper;

  public static Store getStore() {
    Store store = new Store();
    store.setCode("AO001");
    store.setMobileNumber("1234");

    return store;
  }

  public static StoreEntity getStoreEntity() {
    StoreEntity store = new StoreEntity();
    store.setId(1L);
    store.setCode("AO001");
    store.setMobileNumber("1234");

    return store;
  }

  @Before
  public void setUp() {
    initMocks(this);
  }

  @Test
  public void create() {
    // Given
    Store store = getStore();
    StoreEntity entity = getStoreEntity();

    // When
    doNothing().when(storeService)
        .validateUniqueCode(anyString());
    when(mapper.toStoreEntity(store)).thenReturn(entity);
    when(dao.save(entity)).thenReturn(entity);
    Long result = this.storeService.create(store);

    // Then
    Mockito.verify(storeService, Mockito.times(1))
        .validateUniqueCode(anyString());
    Mockito.verify(mapper, Mockito.times(1))
        .toStoreEntity(eq(store));
    Mockito.verify(dao, Mockito.times(1))
        .save(eq(entity));
    Assert.assertEquals("Same Code", entity.getId(), result);
  }

  @Test
  public void update() {
    // Given
    Store store = getStore();
    store.setId(1L);
    StoreEntity entity = getStoreEntity();

    // When
    doNothing().when(storeService)
        .validateEntityExists(anyLong());
    doNothing().when(storeService)
        .validateUniqueCode(anyString());
    when(mapper.toStoreEntity(store)).thenReturn(entity);
    when(dao.save(entity)).thenReturn(entity);
    this.storeService.update(store);

    // Then
    Mockito.verify(storeService, Mockito.times(1))
        .validateUniqueCode(anyString());
    Mockito.verify(storeService, Mockito.times(1))
        .validateEntityExists(anyLong());
    Mockito.verify(mapper, Mockito.times(1))
        .toStoreEntity(eq(store));
    Mockito.verify(dao, Mockito.times(1))
        .save(eq(entity));
  }

  @Test
  public void getById() {
    // Given
    Long id = 1L;
    StoreEntity entity = getStoreEntity();

    // When
    when(dao.findById(id)).thenReturn(Optional.of(entity));
    when(mapper.toStore(entity)).thenReturn(getStore());
    Store result = this.storeService.getById(id);

    // Then
    Mockito.verify(mapper, Mockito.times(1))
        .toStore(eq(entity));
    Assert.assertEquals("Expected", getStore(), result);

  }

  @Test
  public void delete() {
    // Given
    Long id = 1L;
    StoreEntity entity = getStoreEntity();

    // When
    doReturn(getStore()).when(storeService)
        .getById(id);
    when(mapper.toStore(entity)).thenReturn(getStore());
    this.storeService.delete(id);

    // Then
    verify(dao, times(1)).delete(any());
  }

  @Test(expected = ValidationException.class)
  public void testValidateUniqueCodeException() {
    // Given
    String code = "AO01";
    Page stores = mock(Page.class);

    // When
    when(stores.getNumberOfElements()).thenReturn(1);
    doReturn(stores).when(storeService)
        .search(any());
    storeService.validateUniqueCode(code);
  }

  @Test(expected = EntityNotFoundException.class)
  public void testValidateEntityExists() {
    // Given
    Long id = 1L;

    // When
    doReturn(null).when(storeService)
        .getById(id);
    storeService.validateEntityExists(id);
  }
}