package com.grupodot.shoppingcenter.service.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.MockitoAnnotations.initMocks;

import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

public class OpenBankServiceImplTest {

  @InjectMocks
  @Spy
  private OpenBankServiceImpl openBankService;

  @Mock
  private ProducerTemplate producerTemplate;

  @Before
  public void setUp() {
    initMocks(this);
  }


  @Test
  public void testGetProductsUseCamel() {
    // Given

    // When
    openBankService.getProducts();

    // Then
    Mockito.verify(producerTemplate, Mockito.times(1))
        .sendBody(eq("direct:getRestFromOpenBank"), eq(ExchangePattern.InOut), any());
  }
}