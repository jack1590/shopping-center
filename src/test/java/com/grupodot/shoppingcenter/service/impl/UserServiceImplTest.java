package com.grupodot.shoppingcenter.service.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import com.grupodot.shoppingcenter.dao.UserDao;
import com.grupodot.shoppingcenter.dto.User;
import com.grupodot.shoppingcenter.entity.UserEntity;
import com.grupodot.shoppingcenter.enums.DocumentType;
import com.grupodot.shoppingcenter.mapper.UserMapper;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class UserServiceImplTest {

  @InjectMocks
  @Spy
  private UserServiceImpl userService;

  @Mock
  private UserDao dao;

  @Mock
  private UserMapper mapper;

  @Mock
  private BCryptPasswordEncoder bCryptPasswordEncoder;

  public static User getUser() {
    User user = new User();
    user.setPassword("123456");

    return user;
  }

  public static UserEntity getUserEntity() {
    UserEntity user = new UserEntity();
    user.setId(1L);
    user.setPassword("123456");

    return user;
  }

  @Before
  public void setUp() {
    initMocks(this);
  }

  @Test
  public void create() {
    // Given
    User user = getUser();
    UserEntity entity = getUserEntity();

    // When
    doNothing().when(userService)
        .validateUniqueEmail(any());
    doNothing().when(userService)
        .validateIdNumberAndDocumentType(any(), any());

    when(mapper.toUserEntity(user)).thenReturn(entity);
    when(dao.save(entity)).thenReturn(entity);
    Long result = this.userService.create(user);

    // Then
    Mockito.verify(userService, Mockito.times(1))
        .validateIdNumberAndDocumentType(any(), any());
    Mockito.verify(userService, Mockito.times(1))
        .validateUniqueEmail(any());
    Mockito.verify(mapper, Mockito.times(1))
        .toUserEntity(eq(user));
    Mockito.verify(dao, Mockito.times(1))
        .save(eq(entity));
    Assert.assertEquals("Same Code", entity.getId(), result);
  }

  @Test
  public void update() {
    // Given
    User user = getUser();
    user.setId(1L);
    UserEntity entity = getUserEntity();

    // When
    doNothing().when(userService)
        .validateEntityExists(anyLong());
    doNothing().when(userService)
        .validateIdNumberAndDocumentType(any(), any());
    doNothing().when(userService)
        .validateUniqueEmail(any());
    when(mapper.toUserEntity(user)).thenReturn(entity);
    when(dao.save(entity)).thenReturn(entity);
    this.userService.update(user);

    // Then
    Mockito.verify(userService, Mockito.times(1))
        .validateIdNumberAndDocumentType(any(), any());
    Mockito.verify(userService, Mockito.times(1))
        .validateUniqueEmail(any());
    Mockito.verify(userService, Mockito.times(1))
        .validateEntityExists(anyLong());
    Mockito.verify(mapper, Mockito.times(1))
        .toUserEntity(eq(user));
    Mockito.verify(dao, Mockito.times(1))
        .save(eq(entity));
  }

  @Test
  public void getById() {
    // Given
    Long id = 1L;
    UserEntity entity = getUserEntity();

    // When
    when(dao.findById(id)).thenReturn(Optional.of(entity));
    when(mapper.toUser(entity)).thenReturn(getUser());
    User result = this.userService.getById(id);

    // Then
    Mockito.verify(mapper, Mockito.times(1))
        .toUser(eq(entity));
    Assert.assertEquals("Expected", getUser(), result);

  }

  @Test
  public void delete() {
    // Given
    Long id = 1L;
    UserEntity entity = getUserEntity();

    // When
    doReturn(getUser()).when(userService)
        .getById(id);
    when(mapper.toUser(entity)).thenReturn(getUser());
    this.userService.delete(id);

    // Then
    verify(dao, times(1)).delete(any());
  }

  @Test(expected = ValidationException.class)
  public void testValidateIdNumberAndDocumentType() {
    // Given
    DocumentType documentType = DocumentType.CC;
    String idNumber = "d332rr4";
    Page users = mock(Page.class);

    // When
    when(users.getNumberOfElements()).thenReturn(1);
    doReturn(users).when(userService)
        .search(any());
    userService.validateIdNumberAndDocumentType(documentType, idNumber);
  }

  @Test(expected = ValidationException.class)
  public void testValidate() {
    // Given
    String email = "jcarl.30@gmail.com";
    Page users = mock(Page.class);

    // When
    when(users.getNumberOfElements()).thenReturn(1);
    doReturn(users).when(userService)
        .search(any());
    userService.validateUniqueEmail(email);
  }

  @Test(expected = EntityNotFoundException.class)
  public void testValidateEntityExists() {
    // Given
    Long id = 1L;

    // When
    doReturn(null).when(userService)
        .getById(id);
    userService.validateEntityExists(id);
  }
}