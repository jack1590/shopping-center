package com.grupodot.shoppingcenter.service.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import com.grupodot.shoppingcenter.dao.ProductDao;
import com.grupodot.shoppingcenter.dto.Product;
import com.grupodot.shoppingcenter.entity.ProductEntity;
import com.grupodot.shoppingcenter.mapper.ProductMapper;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.data.domain.Page;

public class ProductServiceImplTest {

  @InjectMocks
  @Spy
  private ProductServiceImpl productService;

  @Mock
  private ProductDao dao;

  @Mock
  private ProductMapper mapper;

  public static Product getProduct() {
    Product product = new Product();
    product.setCode("AO001");
    product.setName("Iphone");

    return product;
  }

  public static ProductEntity getProductEntity() {
    ProductEntity product = new ProductEntity();
    product.setId(1L);
    product.setCode("AO001");
    product.setName("Iphone");

    return product;
  }

  @Before
  public void setUp() {
    initMocks(this);
  }

  @Test
  public void create() {
    // Given
    Product product = getProduct();
    ProductEntity entity = getProductEntity();

    // When
    doNothing().when(productService)
        .validateUniqueCode(anyString());
    when(mapper.toProductEntity(product)).thenReturn(entity);
    when(dao.save(entity)).thenReturn(entity);
    Long result = this.productService.create(product);

    // Then
    Mockito.verify(productService, Mockito.times(1))
        .validateUniqueCode(anyString());
    Mockito.verify(mapper, Mockito.times(1))
        .toProductEntity(eq(product));
    Mockito.verify(dao, Mockito.times(1))
        .save(eq(entity));
    Assert.assertEquals("Same Code", entity.getId(), result);
  }

  @Test
  public void update() {
    // Given
    Product product = getProduct();
    product.setId(1L);
    ProductEntity entity = getProductEntity();

    // When
    doNothing().when(productService)
        .validateEntityExists(anyLong());
    doNothing().when(productService)
        .validateUniqueCode(anyString());
    when(mapper.toProductEntity(product)).thenReturn(entity);
    when(dao.save(entity)).thenReturn(entity);
    this.productService.update(product);

    // Then
    Mockito.verify(productService, Mockito.times(1))
        .validateUniqueCode(anyString());
    Mockito.verify(productService, Mockito.times(1))
        .validateEntityExists(anyLong());
    Mockito.verify(mapper, Mockito.times(1))
        .toProductEntity(eq(product));
    Mockito.verify(dao, Mockito.times(1))
        .save(eq(entity));
  }

  @Test
  public void getById() {
    // Given
    Long id = 1L;
    ProductEntity entity = getProductEntity();

    // When
    when(dao.findById(id)).thenReturn(Optional.of(entity));
    when(mapper.toProduct(entity)).thenReturn(getProduct());
    Product result = this.productService.getById(id);

    // Then
    Mockito.verify(mapper, Mockito.times(1))
        .toProduct(eq(entity));
    Assert.assertEquals("Expected", getProduct(), result);

  }

  @Test
  public void delete() {
    // Given
    Long id = 1L;
    ProductEntity entity = getProductEntity();

    // When
    doReturn(getProduct()).when(productService)
        .getById(id);
    when(mapper.toProduct(entity)).thenReturn(getProduct());
    this.productService.delete(id);

    // Then
    verify(dao, times(1)).delete(any());
  }

  @Test(expected = ValidationException.class)
  public void testValidateUniqueCodeException() {
    // Given
    String code = "AO01";
    Page products = mock(Page.class);

    // When
    when(products.getNumberOfElements()).thenReturn(1);
    doReturn(products).when(productService)
        .search(any());
    productService.validateUniqueCode(code);
  }

  @Test(expected = EntityNotFoundException.class)
  public void testValidateEntityExists() {
    // Given
    Long id = 1L;

    // When
    doReturn(null).when(productService)
        .getById(id);
    productService.validateEntityExists(id);
  }
}